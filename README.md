Simplenews Farm
===============

Provides administrative tools for individual lists to individual users, based
on an entity reference field assigned to a newsletter category term on user
account creation.

Deployment
----------

Once you have downloaded and enabled the module, the simplest way to get things
started is to assign the permission administer own lists to the authenticated
user role.

You will need a simplenews enabled content type on your site.

WARNING
-------

When a user is deleted, this module deletes the users associated taxonomy terms, effectively orphaning any newsletters created for those categories.
